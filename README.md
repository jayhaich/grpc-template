# grpc-template

A (very) simple skeleton project for GRPC

The indended workflow is:
- Fork this project using `fork.py`
- Create your service and message definitions here
- Submodule this project into client and server projects and generate the code
  into those projects

# Known Issues
## Golang
### protoc-gen-go: program not found or is not executable
Try adding this to your ~/.bashrc:
```
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOROOT:$GOPATH:$GOBIN
```
